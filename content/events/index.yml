title: Events | GitLab
description: Where you'll find information on the GitLab happenings circuit
header:
  title: GitLab Events
  centered_by_default: true
  text: GitLab is The DevOps Platform that empowers organizations to maximize the overall return on software development. Join an event to learn how your team can deliver software faster and efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events this year, and cannot wait to see you there!
event_videos:
  header: Event Videos
  videos:
    - title: "Commit Virtual - Four Ways to Further FOSS"
      photourl: /nuxt-images/events/1.jpg
      video_link: https://www.youtube.com/embed/8h95PjdUyn4
      carousel_identifier:
        - 'Event Videos'

    - title: "How Delta became truly cloud native"
      photourl: /nuxt-images/events/2.jpg
      video_link: https://www.youtube-nocookie.com/embed/zV_hFcxoN8I
      carousel_identifier:
        - 'Event Videos'

    - title: "The Power of GitLab"
      photourl: /nuxt-images/events/3.jpg
      video_link: https://www.youtube-nocookie.com/embed/tIm643kyQqs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevOps Culture at Porsche - A GitLab success story"
      photourl: /nuxt-images/events/4.jpg
      video_link: https://www.youtube-nocookie.com/embed/O9MdFhaosRo
      carousel_identifier:
        - 'Event Videos'

    - title: "Creating a CI/CD Pipeline with GitLab and Kubernetes in 20 Minutes"
      photourl: /nuxt-images/events/5.jpg
      video_link: https://www.youtube-nocookie.com/embed/-shvwiBwFVI
      carousel_identifier:
        - 'Event Videos'

    - title: "GitLab Product Keynote at Commit SF"
      photourl: /nuxt-images/events/6.jpg
      video_link: https://www.youtube-nocookie.com/embed/lFIk7E38Djs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevSecOps & GitLab's Security Solutions"
      photourl: /nuxt-images/events/7.jpg
      video_link: https://www.youtube-nocookie.com/embed/CjX1TsCZgoQ
      carousel_identifier:
        - 'Event Videos'

    - title: "Gitlab Connect Paris 2019"
      photourl: /nuxt-images/events/8.jpg
      video_link: https://www.youtube-nocookie.com/embed/YwzpNNSdx_I
      carousel_identifier:
        - 'Event Videos'

    - title: "Verizon Connect achieves datacenter deploys in under 8 hours with GitLab"
      photourl: /nuxt-images/events/9.jpg
      video_link: https://www.youtube-nocookie.com/embed/zxMFaw5j6Zs
      carousel_identifier:
        - 'Event Videos'

    - title: "DataOps in a Cloud Native World"
      photourl: /nuxt-images/events/10.jpg
      video_link: https://www.youtube-nocookie.com/embed/PLe9sovhtGA
      carousel_identifier:
        - 'Event Videos'

events:

  - topic: All Things Open
    type: Conference
    date_starts: November 1, 2022
    date_ends: November 2, 2022
    description: Come join GitLab at All Things Open, a technology conference focusing on the tools, processes, and people making open source possible.
    location: Raleigh, NC
    region: AMER
    social_tags:
    event_url: https://2021.allthingsopen.org/save-the-date-2022/

  - topic: KubeCon + CloudNativeCon North America 2022
    type: Conference
    date_starts: October 24, 2022
    date_ends: October 28, 2022
    description: |
      Join the GitLab team at Booth P21 and Activation Zone 3 for a whole array of fun activities! We'll have exciting lightning talks throughout the week, you can try your hand at our Code Challenge, take a photo for our Everyone Can Contribute photo mosaic wall, or get exclusive GitLab swag. GitLab will be co-sponsoring the KubeCruise North America ancillary party with partners at the Detroit Princess Riverboat (registration link coming soon!). Come listen to one of our [3 speaking sessions](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/program/schedule/):
      1. Brendan O’Leary, Staff Developer Evangelist at GitLab, GitLab: Consumers to Contributors: Open source as a competitive advantage
      2. Andrew Newdigate, Distinguished Engineer, Infrastructure at GitLab: How GitLab.com uses long-term monitoring data for capacity forecasting.
      3. Abubakar Siddiq Ango, Developer Evangelism Program Manager at GitLab: What Container Runtime do I need?
    location: Detroit, MI
    region: AMER
    social_tags:
      - KubeCon
      - CloudNativeCon
    event_url: https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/

  - topic: Big Data Conference Europe 2022
    type: Conference
    date_starts: November 23, 2022
    date_ends: November 24, 2022
    description: Big Data Conference Europe is a two-day conference with technical talks in the fields of Big Data, High Load, Data Science, Machine Learning and AI. Conference brings together developers, IT professionals and users to share their experience, discuss best practices, describe use cases and business applications related to their successes. Our Data team member will give an overview of how our **Data team** does the thing and how they use our DevOps product in their daily work to achieve exceptional results in a fast-paced environment. You will hear interesting story of the process of creating, improving, and scaling the Data product using a modern DevOps stack. Exposing details of the use case how we embrace the open-source philosophy to help us provide faster time to market. Will have a chance to discuss how to use the advantage of the internal product to make us more agile in the daily job of creating great data products.
    location: Virtual
    region: EMEA
    social_tags:
      - bigdataconference
    event_url: https://bigdataconference.eu/

  - topic: DevOpsDays Chicago
    type: Conference
    date_starts: September 21, 2022
    date_ends: September 22, 2022
    description: GitLab will be at DevOpsDays Chicago which brings development, operations, QA, InfoSec, management, and leadership together to discuss the culture and tools to make better organizations and products.
    location: Chicago, IL
    region: AMER
    social_tags:
    event_url: https://devopsdays.org/events/2022-chicago/welcome/

  - topic: Container Days EU
    type: Conference
    date_starts: September 5, 2022
    date_ends: September 7, 2022
    description: Join us in Hamburg, Michael Friedrich will be seaking about "Confidence with Chaos for your Kubernetes Observability". ContainerDays 2022 will offer a great learning experience on Kubernetes, CloudNative, DevOps, GitOps, EdgeComputing and much more.
    location: Hamburg, Germany and virtual
    region: EMEA
    social_tags:
      - ContainerDays
    event_url: https://www.containerdays.io/

  - topic: EMERGE 2022 - Forum on the Future of AI Driven Humanity & International Conference Digital Society Now
    type: Conference
    date_starts: December 16, 2022
    date_ends: December 18, 2022
    description: EMERGE is an annual event organised by the Digital Society Lab of the Institute for Philosophy and Social Theory, University of Belgrade. Its goal is to connect actors from the tech industry, policy makers, and academic researchers in discussing the social and economic impact of emerging technologies. EMERGE 2022 will consist of the EMERGE Forum on The Future of AI driven Humanity and the International Scientific Conference on Digital Society Now. How GitLab Data team embraces DevOps culture to move fast in a rapidly growing environment.Why you should find solutions for your challenges in the Open Source world. What are the vital points you should stick with in order to provide trusted data on time for your users. Providing a walkthrough over the process of creating, improving, and scaling the Data product using a modern DevOps stack in the Open Source world. Exposing details of the use case and how we embrace the open-source philosophy to help us provide faster time to market. We will discuss how to use the advantage of the internal product to make us more agile in the daily job of creating great data products.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
      - emerge2022
    event_url: https://emerge.ifdt.bg.ac.rs/

  - topic: AWS Summit Canberra
    type: Conference
    date_starts: August 31, 2022
    date_ends: August 31, 2022
    description: Join GitLab at AWS Summit Canberra to learn how GitLab + AWS can help you deploy to the cloud! We are an exhibitor level sponsor...check back for our booth number!
    location: Canberra, ACT
    region: APAC
    social_tags:
    event_url: https://aws.amazon.com/events/summits/canberra/

  - topic: GitLab Security + Compliance Workshop
    type: Webcast
    date_starts: September 12, 2022
    date_ends: September 12, 2022
    description: Cyber attacks have never been more in the news. From Twitter hacks to identity theft, vulnerabilities are exposing gaps in the application development process. Application security is difficult, especially when security is a separate process from your DevOps workflow. Security has traditionally been the final hurdle to conquer in the development lifecycle. Join this three hour hands-on workshop to gain a better understanding of how to successfully shift security left to find and fix security flaws during development - and to do so more easily and with greater visibility and control than typical approaches can provide.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/amersecurityws-virtual-registration-page.html

  - topic: Gartner 2022 APAC IT Symposium/Xpo™
    type: Conference
    date_starts: September 12, 2022
    date_ends: September 14, 2022
    description: GitLab is looking forward to connecting with the Gartner Symposium attendees at this event. Our booth number has not been given out at this time, but check back closer to time for more information!
    location: Gold Coast, QLD
    region: APAC
    social_tags:
    event_url: https://www.gartner.com/en/conferences/apac/symposium-australia

  - topic: Devopsdays Berlin 2022
    type: Conference
    date_starts: September 21, 2022
    date_ends: September 22, 2022
    description: Devopsdays is a worldwide series of technical conferences covering topics of software development, IT infrastructure operations, and the intersection between them. Each event is run by volunteers from the local area. Happy to tell the GitLab story of how we embrace DevOps and Open Source in the (officially) biggest all-remote company in the world to do magical things and move fast in the Data World. Thrilled to share our experience from the Data Team with the broader audience about how to move fast, stay focused on the success journey and orchestrate team members around the globe. Also, will share the secret sauce on how to start fully transparent and keep the startup vibrance in the fast-growing company.
    location: Berlin, Germany
    region: EMEA
    social_tags:
      - devopsdays
    event_url: https://devopsdays.org/events/2022-berlin/welcome/

  - topic: The Geek Gathering
    type: Conference
    date_starts: October 6, 2022
    date_ends: October 7, 2022
    description: Take in the experts' perspectives, discuss strategies, and get a grip on diverse problem-solving approaches. Gathering designed for everyone who geeks out over fresh outlooks from the tech community and isn't keen on sales tactics in disguise. Our Data team member is thrilled to tell the GitLab story of how we embrace DevOps and Open Source in the all-remote company to do magical things and move fast in the Data World. Will share our Data Team's experience about how to move fast, stay focused on the success journey and orchestrate team members around the globe. Also, will reveal the secret sauce on how to start fully transparent and keep the startup vibrance in the fast-growing company.
    location: Osijek, Croatia
    region: EMEA
    social_tags:
      - thegeekgathering
    event_url: https://www.thegeekgathering.org/

  - topic: CyberSecurity Workshop for the Public Sector
    type: Webcast
    date_starts: September 22, 2022
    date_ends: September 22, 2022
    description: Rapid iterations of DevOps, along with a host of new tools, can make an application security program seem like a square peg in a round hole as enterprises try to push Sec into the middle of DevOps. At the same time, modern applications rely on a more dynamic environment that can introduce new CyberSecurity challenges, particularly as they scale. Join this three hour hands-on workshop to gain a better understanding of how to successfully shift security left to find and fix security flaws during development - and to do so more easily and with greater visibility and control than typical approaches can provide.
    location: Virtual
    region: PubSec
    social_tags:
    event_url: https://page.gitlab.com/cybersecps-virtual-registration-page.html

  - topic: IDC IT Security Conference
    type: Conference
    date_starts: September 27, 2022
    date_ends: September 27, 2022
    description: Join GitLab at IDC's IT Security Conference in Stockholm on the 27th of September, to share experinces and learn new techniques with other secuirty leaders.
    location: Stockholm, Sweden
    region: EMEA
    social_tags:
    event_url: https://www.idc.com/eu/events/69929-idc-it-security-2022


  - topic: Cloud Expo Asia
    type: Conference
    date_starts: October 12, 2022
    date_ends: October 13, 2022
    description: Visit GitLab at Cloud Expo Asia, Booth G75!
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://www.cloudexpoasia.com/

  - topic: WWT Happy Hour Seattle
    type: Conference
    date_starts: September 15, 2022
    date_ends: September 15, 2022
    description:  Please join GitLab along with our partner WWT for an in-person Happy Hour event in Bellevue Washington at 6pm, Thursday, September 15th, 2022.  Enjoy an evening panel discussion that will cover how security leaders are solving real world cloud and DevOps problems. While you network we will be offering snacks, light appetizers and of course something to quench your thirst!
    location: Belleview, WA
    region: AMER
    social_tags:
    event_url: https://info.lacework.com/pnw-happy-hour-with-wwt-gitlab-and-cribl.html?utm_source=partner&utm_medium=email&utm_campaign=20220915_AMER_US_FEP_PNW_Happy_Hour_with_WWT_GitLab_and_Cribl&utm_partner=GitLab

  - topic: GitLab Advanced CI/CD Virtual Workshop
    type: Webcast
    date_starts: October 25, 2022
    date_ends: October 25, 2022
    description: GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. You might have had the chance to take it for a test drive, but even our most advanced users know there’s still a lot to learn to push better and faster automation throughout your DevOps lifecycle! We’re inviting you to a deep-dive workshop on Advanced GitLab CI/CD, including everything you need to enable you to take your automation game to the next level, and provide thought leadership within your organization.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/advanced-cicd-virtual-registration-page.html

  - topic: Educause Annual Conference 2022
    type: Conference
    date_starts: October 25, 2022
    date_ends: October 28, 2022
    description: With the best presenters, the best content, and the best networking, this premier higher ed IT event brings together professionals and technology providers from around the world to discuss their discoveries, grow professionally, and explore solutions to continuing challenges. GitLab is a proud sponsor of this year's event!
    location: Denver, CO
    region: PubSec
    social_tags:
    event_url: https://events.educause.edu/annual-conference

  - topic: TechX Summit
    type: Conference
    date_starts: November 3, 2022
    date_ends: November 3, 2022
    description: Join GitLab on November 3, 2022 for an immersive day of peer on peer learning and Netwoking with over 150 like minded C level executives.
    location: Dubai, United Arab Emirates
    region: EMEA
    social_tags:
    event_url: https://www.techx-summit.com/


  - topic: TNIP 2022
    type: Conference
    date_starts: November 1, 2022
    date_ends: November 3, 2022
    description: TechNet Indo-Pacific is co-sponsored by AFCEA International and AFCEA Hawaii. It is the largest event in the Pacific Rim focusing on regional defense issues. Keynote speakers, panel moderators and panelists will discuss defense policies and challenges in the Indo- Pacific region and their relevance to both industry and government. Industry exhibitors will demonstrate products and solutions to meet the services' requirements and needs. Visit GitLab at booth 507!
    location: Honolulu, HI
    region: PubSec
    social_tags:
    event_url: https://www.afcea.org/calendar/eventdet.jsp?event_id=45577


  - topic: Innovate NZ
    type: Conference
    date_starts: November 10, 2022
    date_ends: November 10, 2022
    description: GitLab is looking forward to connecting with the Innovate NZ attendees at this event. Our booth number has not been given out at this time, but check back closer to time for more information!
    location: Wellington, NZ
    region: APAC
    social_tags:
    event_url: https://publicsectornetwork.co/event/innovate-nz-2022/

  - topic: Cloud Expo Paris
    type: Conference
    date_starts: November 16, 2022
    date_ends: November 17, 2022
    description: Join GitLab at the largest business show in France dedicated to the Cloud, Cyber ​​Security, DevOps, Big Data & IA and new technologies. Visit GitLab at Booth F70
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://www.cloudexpoeurope.fr/

  - topic: Digital NSW
    type: Conference
    date_starts: November 30, 2022
    date_ends: November 30, 2022
    description: GitLab is looking forward to connecting with the Australian Government attendees at this event. Our booth number has not been given out at this time, but check back closer to time for more information!
    location: Sydney, NSW
    region: APAC
    social_tags:
    event_url: https://publicsectornetwork.co/event/digital_nsw_2022/


  - topic: Tech Rocks Summit Paris 2022
    type: Conference
    date_starts: December 8, 2022
    date_ends: December 9, 2022
    description: Join GitLab at the Tech.Rocks Summit which is back as a hybrid event! Come and enjoy the event for tech leaders and network with your peers.
    location: Paris, France
    region: EMEA
    social_tags:
    event_url: https://www.tech.rocks/

  - topic: EMERGE 2022 - Forum on the Future of AI Driven Humanity & International Conference Digital Society Now
    type: Conference
    date_starts: December 16, 2022
    date_ends: December 18, 2022
    description: EMERGE is an annual event organised by the Digital Society Lab of the Institute for Philosophy and Social Theory, University of Belgrade. Its goal is to connect actors from the tech industry, policy makers, and academic researchers in discussing the social and economic impact of emerging technologies. EMERGE 2022 will consist of the EMERGE Forum on The Future of AI driven Humanity and the International Scientific Conference on Digital Society Now. How GitLab Data team embraces DevOps culture to move fast in a rapidly growing environment.Why you should find solutions for your challenges in the Open Source world. What are the vital points you should stick with in order to provide trusted data on time for your users. Providing a walkthrough over the process of creating, improving, and scaling the Data product using a modern DevOps stack in the Open Source world. Exposing details of the use case and how we embrace the open-source philosophy to help us provide faster time to market. We will discuss how to use the advantage of the internal product to make us more agile in the daily job of creating great data products.
    location: Belgrade, Serbia
    region: EMEA
    social_tags:
      - emerge2022
    event_url: https://emerge.ifdt.bg.ac.rs/

  - topic: Dev Day Workshop - Deploy, Manage, and Scale Kubernetes to AWS EKS with GitLab
    type: Conference
    date_starts: October 11, 2022
    date_ends: October 11, 2022
    description: This 4-hour hands-on workshop is designed for development teams planning projects involving AWS EKS clusters. Take your Kubernetes skills to the next level! Join industry-leading experts from AWS and GitLab for a hands-on workshop that will enable you to deploy secure applications to AWS Elastic Kubernetes Service in record time.
    location: Palo Alto, CA
    region: AMER
    social_tags:
    event_url: https://page.gitlab.com/awsworkshoppaloalto-in-person-registration-page.html?utm_medium=email&utm_source=marketo&utm_campaign=20221011awsworkshoppaloaltofmm

  - topic: DevOps Automation Virtual Workshop for the Public Sector
    type: Webcast
    date_starts: October 27, 2022
    date_ends: October 27, 2022
    description: Uncover how the most effective Application Development teams leverage GitLab to automate their DevOps process and achieve material business outcomes. In this 3-hour simulated team environment, attendees will respond to various scenarios utilizing GitLab's single application for the entire DevOps lifecycle. No previous experience with GitLab is required.
    location: Virtual
    region: PubSec
    social_tags:
    event_url: https://page.gitlab.com/devopsautps-virtual-registration-page.html

  - topic: DevOpsDays Houston
    type: Conference
    date_starts: October 4, 2022
    date_ends: October 5, 2022
    description: GitLab will be at DevOpsDays Houston which brings development, operations, QA, InfoSec, management, and leadership together to discuss the culture and tools to make better organizations and products.
    location: Houston, TX
    region: AMER
    social_tags:
    event_url: https://devopsdays.org/events/2022-houston/welcome/

  - topic: TechTalk - DORA Metrics with GitLab
    type: Webcast
    date_starts: October 18, 2022
    date_ends: October 18, 2022
    description: Join us as we take a deep dive into the art of the possible with DORA metrics and beyond. This session will go over what DevOps metrics are available and arm you with the knowledge needed to build out a best practices metrics program, achieve visibility across your team in order to improve efficiencies, and leverage better insights on your software development to improve cycle times. Bring your questions and projects to this interactive session. Attendees will have the option to sign up for a delivered beverage and snack of choice.
    location: Virtual
    region: PubSec
    social_tags:
    event_url: https://www.meetup.com/gitlab-government-user-group/events/287837327/

  - topic: Ruby on Rails Global Summit 2023 - How GitLab hires Ruby on Rails Engineers
    type: Conference
    date_starts: January 24, 2023
    date_ends: January 25, 2023
    description: This talk covers how GitLab hires Ruby on Rails engineers including sourcing candidates, choosing which to screen, technical evaluation, behavioral evaluation, reference check, background check, and offer. It should be enlightening for both developers and for hiring managers.
    location: Virtual
    region: AMER
    social_tags:
    event_url: https://events.geekle.us/ruby/
